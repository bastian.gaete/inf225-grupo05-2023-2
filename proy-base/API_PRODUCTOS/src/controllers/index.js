import prisma from "../db.js";
import { v4 as uuidV4 } from "uuid";

export async function getProducts() {
  try {
    return await prisma.product.findMany({
      include: { categories: { include: { types: true } } },
    });
  } catch (error) {
    throw new Error("Error al obtener productos");
  }
}

export async function getProduct(id) {
  try {
    return await prisma.product.findUnique({
      where: {
        id,
      },
      include: { categories: { include: { types: true } } },
    });
  } catch (error) {
    throw new Error("Error al obtener el producto");
  }
}

export async function createProduct(data) {
  try {
    return await prisma.product.create({ data: { ...data, id: uuidV4() } });
  } catch (error) {
    throw new Error("Error al crear el producto");
  }
}

export async function updateProduct(id, data) {
  try {
    return await prisma.product.update({ data, where: { id } });
  } catch (error) {
    throw new Error("Error al actualizar el producto");
  }
}

export async function deleteProduct(id) {
  try {
    return await prisma.product.delete({ where: { id } });
  } catch (error) {
    throw new Error("Error al eliminar el producto");
  }
}

export async function getCategories() {
  try {
    return await prisma.category.findMany({ include: { types: true } });
  } catch (error) {
    throw new Error("Error al obtener categorías");
  }
}

export async function createCategory(data) {
  try {
    return await prisma.product.create({ data });
  } catch (error) {
    throw new Error("Error al crear el producto");
  }
}
