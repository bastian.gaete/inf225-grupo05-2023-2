import express from "express";
import * as controller from "../controllers/index.js";
const router = new express.Router();

router.get("/products", async (req, res) => {
  if (req.params.id) {
    return controller
      .getProduct(req.params.id)
      .then((product) => res.status(200).json(product))
      .catch((error) => res.status(500).send(error));
  } else {
    return controller
      .getProducts()
      .then((products) => res.status(200).json(products))
      .catch((error) => res.status(500).send(error));
  }
});

router.post("/products", async (req, res) => {
  try {
    const body = req.body;
    return controller
      .createProduct(body)
      .then((product) => res.status(200).json(product))
      .catch((error) => res.status(500).send(error));
  } catch (error) {
    return res.status(400).send("Bad request");
  }
});

router.put("/products", async (req, res) => {
  try {
    const body = req.body;
    return controller
      .updateProduct(body)
      .then((product) => res.status(200).json(product))
      .catch((error) => res.status(500).send(error));
  } catch (error) {
    return res.status(400).send("Bad request");
  }
});

router.get("/categories", async (_, res) => {
  return controller
    .getCategories()
    .then((categories) => res.status(200).json(categories))
    .catch((error) => res.status(500).send(error));
});

router.post("/categories", async (req, res) => {
  try {
    const body = req.body;
    return controller
      .createCategory(body)
      .then((category) => res.status(200).json(category))
      .catch((error) => res.status(500).send(error));
  } catch (error) {
    return res.status(400).send("Bad request");
  }
});

export default router;
