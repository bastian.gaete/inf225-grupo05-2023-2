import express from "express";
const app = express();
import "dotenv/config";
import routes from "./src/routes/index.js";

app.use(express.json());
app.use(routes);
app.use((_, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.listen(process.env.PORT_API, () => {
  console.log(`Server running on port ${process.env.PORT_API}!`);
});
