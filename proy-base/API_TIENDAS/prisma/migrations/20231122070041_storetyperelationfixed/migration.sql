/*
  Warnings:

  - You are about to drop the `_StoreToType` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `type_id` to the `Store` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `_StoreToType` DROP FOREIGN KEY `_StoreToType_A_fkey`;

-- DropForeignKey
ALTER TABLE `_StoreToType` DROP FOREIGN KEY `_StoreToType_B_fkey`;

-- AlterTable
ALTER TABLE `Store` ADD COLUMN `type_id` VARCHAR(191) NOT NULL;

-- DropTable
DROP TABLE `_StoreToType`;

-- AddForeignKey
ALTER TABLE `Store` ADD CONSTRAINT `Store_type_id_fkey` FOREIGN KEY (`type_id`) REFERENCES `Type`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
