-- AlterTable
ALTER TABLE `Store` MODIFY `updatedAt` DATETIME(3) NULL;

-- AlterTable
ALTER TABLE `Type` MODIFY `updatedAt` DATETIME(3) NULL;

-- AlterTable
ALTER TABLE `User` MODIFY `updatedAt` DATETIME(3) NULL;
