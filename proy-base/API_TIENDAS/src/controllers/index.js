import prisma from "../db.js";

import { v4 as uuidV4 } from "uuid";

export function getStores() {
  try {
    const stores = prisma.store.findMany();
    return stores;
  } catch (error) {
    throw new Error("Error al obtener las tiendas.");
  }
}

export async function getStore(id) {
  try {
    const store = await prisma.store.findUnique({
      where: {
        id,
      },
      include: {
        type: true,
        links: true,
      },
    });
    return store;
  } catch (error) {
    throw new Error("Error al obtener la tienda.");
  }
}

export async function createStore(data) {
  try {
    const { type, ...storeData } = data;

    const typeInstance = await prisma.type.findFirstOrThrow({
      where: {
        name: type,
      },
    });

    storeData.id = uuidV4();
    storeData.typeId = typeInstance.id;

    // Crea la tienda con el typeId configurado.
    const newStore = await prisma.store.create({
      data: storeData,
      include: {
        links: true,
      },
    });

    return newStore;
  } catch (error) {
    throw new Error("Error al crear la tienda.");
  }
}

export async function updateStore(id, data) {
  return prisma.store.update({
    data,
    where: {
      id,
    },
  });
}

export async function deleteStore(id) {
  return prisma.store.delete({
    where: {
      id,
    },
  });
}

export async function getTypes() {
  return prisma.type.findMany();
}

export async function createType(name) {
  const type = await prisma.type.create({
    data: {
      id: uuidV4(),
      name,
    },
  });
  return type;
}

export async function deleteType(id) {
  return prisma.type.delete({
    where: {
      id,
    },
  });
}

export async function getUser(email, pwd) {
  try {
    const user = await prisma.user.findFirstOrThrow({
      where: {
        email,
        password: pwd,
      },
    });
    const { password, ...response } = user;
    return response;
  } catch (error) {
    console.log(error);
    throw new Error("Error al obtener el usuario.");
  }
}

export async function createUser(data) {
  try {
    const user = await prisma.user.create({
      data: {
        id: uuidV4(),
        ...data,
      },
    });

    return user;
  } catch (error) {
    console.log(error);
    throw new Error("Error al crear el usuario.");
  }
}

export async function setUserRole(id, role) {
  try {
    const user = await prisma.user.update({
      where: {
        id,
      },
      data: {
        role,
      },
    });

    return user;
  } catch (error) {
    throw new Error("Error al actualizar el rol del usuario.");
  }
}

export async function deleteUser(id) {
  try {
    const user = await prisma.user.delete({
      where: {
        id,
      },
    });

    return user;
  } catch (error) {
    throw new Error("Error al eliminar el usuario.");
  }
}

export async function getMessages(senderId, receiverId) {
  const messages = await prisma.message.findMany({
    where: {
      senderId,
      receiverId,
    },
    include: {
      sender: {
        select: {
          name: true,
          id: true,
        },
      },
      receiver: {
        select: {
          name: true,
          id: true,
        },
      },
    },
  });

  return messages;
}

export async function createMessage(message) {
  try {
    const response = await prisma.message.create({
      data: message,
    });
    return response;
  } catch (error) {
    console.log(error);
    throw new Error("Error al enviar mensaje");
  }
}
