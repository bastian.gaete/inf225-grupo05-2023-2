import express from "express";
import * as controller from "../controllers/index.js";
const router = new express.Router();

router.get("/stores", async (req, res) => {
  const { id } = req.params;

  if (!id)
    return controller
      .getStores()
      .then((stores) => {
        return res.status(200).json(stores);
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send("Error getting stores");
      });

  return controller
    .getStore(id)
    .then((store) => {
      return res.status(200).json(store);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send("Error getting store");
    });
});

router.post("/stores", async (req, res) => {
  const { body } = req;

  try {
    const { name, description, address, type } = body;
    return controller
      .createStore({ name, description, address, type })
      .then((store) => {
        return res.status(200).json(store);
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).send("Error creating store");
      });
  } catch {
    return res.status(400).send("Error creating store");
  }
});

router.get("/types", async (_, res) => {
  return controller
    .getTypes()
    .then((types) => {
      return res.status(200).json(types);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send("Error getting types");
    });
});

router.post("/types", async (req, res) => {
  const { body } = req;
  const { name } = body;

  if (!name) return res.status(400).send("Error creating type: missing name");

  return controller
    .createType(name)
    .then((type) => {
      return res.status(200).json(type);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send("Error creating type");
    });
});

router.get("/users", async (req, res) => {
  const { email, password } = req.query;

  if (!email || !password)
    return res.status(400).send("Bad request: missing email or password");
  return controller
    .getUser(email, password)
    .then((user) => res.status(200).json(user));
});

router.post("/users", async (req, res) => {
  const { body } = req;

  return controller
    .createUser(body)
    .then((user) => {
      return res.status(200).json(user);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send("Error creating user");
    });
});

router.put("/users", async (req, res) => {
  const { body } = req;

  if (!body.role || !body.id)
    return res.status(400).send("Bad request: missing email or password");

  return controller
    .setUserRole(body.id, body.role)
    .then((user) => {
      return res.status(200).json(user);
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).send("Error updating user");
    });
});

router.get("/messages", async (req, res) => {
  const { senderId, receiverId } = req.params;

  if (!senderId || !receiverId)
    return res.status(400).send("Bad request: missing sender or receiver ids");

  return controller
    .getMessages(senderId, receiverId)
    .then((messages) => res.status(200).json(messages));
});

router.post("/messages", async (req, res) => {
  try {
    const body = req.body;
    return controller
      .createMessage(body)
      .then((message) => res.status(200).json(message));
  } catch (error) {
    return res.status(500).send("No se ha podido crear el mensaje");
  }
});

export default router;
