import "dotenv/config";
import express from "express";
import router from "./src/routes/index.js";

const app = express();

app.use((_, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.use(express.json());
app.use(router);

app.listen(process.env.PORT_API, () => {
  console.log(`Server running on port ${process.env.PORT_API}!`);
});
