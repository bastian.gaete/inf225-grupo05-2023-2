export type Category = {
    type: string,
    name: string,
    image: string,
    sub_categories?: Omit<Category, 'type'>[]
}