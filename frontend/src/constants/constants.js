export const MAX_PRICE = 1000000;
export const API_TIENDAS = "http://localhost:8081";
export const API_PRODUCTOS = "http://localhost:8080";
export const USER_ROLES = {
  COLLABORATIVE_MANAGER: "gestor_colaborativo",
  ENTREPRENEUR: "emprendedor",
  USER: "user",
};
