import React from 'react'

function GridView({children}) {
  return (
    <div className='w-full max-w-7xl grid gap-4 grid-cols-3 place-items-center'>{children}</div>
  )
}

export default GridView