import { useContext } from "react";
import { DropDownContext } from "../context/DropdownContext";
import {IoIosArrowDown} from "react-icons/io";
import DropdownItem from "./DropdownItem";


const MultilevelDropdown = ({ items, enabled }) => {
  const {
    menuOpen,
    setMenuOpen,
    selected
  } = useContext(DropDownContext)

  return (
    <div className="relative">
      <button
        className="px-2 py-1 bg-neutral-600 rounded w-[10rem] text-left flex"
        onClick={() => setMenuOpen((prev) => !prev)}
      >
        {selected ? selected.name : "Select an option"}
        <IoIosArrowDown className="self-center ml-auto text-neutral-950"/>
      </button>
      {menuOpen && enabled && (
        <ul className="absolute flex flex-col rounded border border-neutral-500 z-30 overflow-x-visible" onMouseLeave={() => {setMenuOpen(false)}}>
          {items.map((item, index) => (
            <DropdownItem
              depth={1}
              key={index}
              item={item}
            />
          ))}
        </ul>
      )}
    </div>
  );
};

export default MultilevelDropdown;