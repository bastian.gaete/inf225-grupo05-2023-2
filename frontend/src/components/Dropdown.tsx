import React, { useRef, useEffect } from 'react'

function Dropdown({options, onSelect} : {options: string[], onSelect: (value: string) => void}) {
  const ref = useRef<HTMLSelectElement>(null)
  useEffect(() => {
    const element = ref.current
    if (element) {
      element.addEventListener('change', () => {
        onSelect(element.value)
      })
    }
    return () => {
      if (element) {
        element.removeEventListener('change', () => {})
      }
    }
  }, [ref])
  return (
    <select className='px-2 py-1 bg-neutral-600 rounded' ref={ref}>
        <option value="all">All</option>
        {
            options.map(
                (option, index) => (
                    <option key={index} value={option}>{option}</option>
                )
            )
        }
    </select>
  )
}

export default Dropdown