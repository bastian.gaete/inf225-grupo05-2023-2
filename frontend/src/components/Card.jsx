function Card({children, image}){
    return (
        <div className="bg-neutral-100 rounded-lg w-[20rem] aspect-video relative overflow-hidden snap-center shrink-0 z-0">
            <img alt="card-img" src={image} className="object-cover"/>
            <div className="absolute left-0 bottom-0">
                {children}
            </div>
        </div>
    )
}

export default Card;