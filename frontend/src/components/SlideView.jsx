function SlideView({children}) {
  return (
    <div className='flex snap-mandatory snap-x gap-4 overflow-y-hidden overflow-x-auto w-full max-w-7xl'>{children}</div>
  )
}

export default SlideView