import { useContext, useRef, useState } from 'react'
import { DropDownContext } from '../context/DropdownContext';

const DropdownItem = ({ item, depth }) => {
    const [open, setOpen] = useState(false);
    const { selectAndClose, childrenKey, setSelectedCategories, selectedCategories, callback} = useContext(DropDownContext)
    const submenuRef = useRef(null);
  
    const handleMouseEnter = () => {
      setOpen(true);
      setSelectedCategories((prev) => {
          if (!prev.length) return [item];
          const aux = [...prev]
          aux[depth - 1] = item;
          return aux;
      })
    };
  
    return (
      <li
        className="relative bg-neutral-600"
        onMouseEnter={handleMouseEnter}
        onMouseLeave={() => {
          setOpen(false)
        }}
      >
        <button
          className={`hover:bg-indigo-600/10 w-full h-full p-2 text-left`}
          onClick={() => {
              selectAndClose(item)
              callback(selectedCategories)
          }}
        >
          {item.name}
        </button>
        {open && item[childrenKey] && (
          <ul
            ref={submenuRef}
            className="absolute left-full top-0 border border-neutral-500 rounded-md z-30  overflow-x-visible  overflow-y-auto max-h-80"
            onMouseLeave={() => {
              setSelectedCategories((prev) => {
                  const rest = prev.length > 1 ? [...prev].pop : prev;
                  return rest;
              })
            }}
          >
            {item[childrenKey].map((child, index) => (
              <DropdownItem
                depth={depth + 1}
                key={index}
                item={child}
              />
            ))}
          </ul>
        )}
      </li>
    );
  };

export default DropdownItem;