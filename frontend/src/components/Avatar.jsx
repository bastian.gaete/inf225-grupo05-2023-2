import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import useSession from "../hooks/useSession";
import { USER_ROLES } from "../constants/constants";

function Avatar() {
  const [menuOpen, setMenuOpen] = useState(false);
  const [session, , , logout] = useSession();
  const navigate = useNavigate();
  return (
    <button
      className="relative"
      onClick={() => {
        setMenuOpen((prev) => !prev);
      }}
    >
      <img className="w-10 ml-10 aspect-square rounded-full bg-white" alt="" />
      <small className="bg-red-600 rounded-full w-4 h-4 aspect-square absolute bottom-0 right-0 text-center align-middle">
        8
      </small>
      {menuOpen && (
        <ul className="absolute right-2 z-30 bg-neutral-800 rounded-b-md rounded-l-md flex flex-col overflow-hidden">
          <Link
            className="break-keep hover:bg-neutral-700 py-2 px-3"
            to="/perfil"
          >
            Mi Perfil
          </Link>
          {(session?.role === USER_ROLES.ENTREPRENEUR ||
            session.role === USER_ROLES.COLLABORATIVE_MANAGER) && (
            <Link
              className="break-keep hover:bg-neutral-700 py-2 px-3"
              to="/mis-tiendas"
            >
              Mis Tiendas
            </Link>
          )}
          {session.role === USER_ROLES.COLLABORATIVE_MANAGER && (
            <Link
              className="break-keep hover:bg-neutral-700 py-2 px-3"
              to="/gestor"
            >
              Gestionar Tiendas
            </Link>
          )}
          <Link
            className="break-keep hover:bg-neutral-700 py-2 px-3"
            to="/mensajes"
          >
            Mensajes
          </Link>
          <button
            onClick={() => {
              logout().then(() => {
                navigate("/login");
              });
            }}
            className="break-keep hover:bg-neutral-700 py-2 px-3"
          >
            Cerrar Sesión
          </button>
        </ul>
      )}
    </button>
  );
}

export default Avatar;
