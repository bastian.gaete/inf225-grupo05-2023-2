import { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";

function NavLink({ text, to, method }) {
  const location = useLocation();
  const normalizedText = text
    ?.normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase();

  useEffect(() => {
    if (method) {
      method();
    }
  }, [method]);

  return (
    <li
      className={`px-2 py-1 rounded hover:bg-neutral-800 text-lg ${
        location.pathname?.slice(1) === normalizedText
          ? "font-semibold"
          : "font-normal opacity-80"
      }`}
    >
      <Link to={to}>{text}</Link>
    </li>
  );
}

export default NavLink;
