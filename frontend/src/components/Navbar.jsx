import useSession from "../hooks/useSession";
import Avatar from "./Avatar";
import NavLink from "./NavLink";

function Navbar() {
  const [session] = useSession();

  return (
    <div className="w-full h-20 bg-neutral-950 px-10 flex place-items-center text-white absolute">
      <h2 className="text-violet-500 text-2xl font-bold">
        Tienda Colaborativa
      </h2>
      <nav className="ml-auto">
        {session && (
          <ul className="flex gap-x-5">
            <NavLink text="Categorías" to="/categorias" />
            <NavLink text="Tiendas" to="/tiendas" />
            <NavLink text="Productos" to="/productos" />
          </ul>
        )}
      </nav>
      {session && <Avatar />}
    </div>
  );
}

export default Navbar;
