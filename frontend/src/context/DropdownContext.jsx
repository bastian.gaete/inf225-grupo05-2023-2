import { createContext, useState } from "react";

export const DropDownContext = createContext(null)
export function DropDownContextProvider({children, childrenKey, callback}){
    const [menuOpen, setMenuOpen] = useState(false);
    const [focused, setFocused] = useState(null);
    const [selected, setSelected] = useState(null);
    const [selectedCategories, setSelectedCategories] = useState([]);

    const selectAndClose = (item) => {
        setSelected(item);
        setMenuOpen(false);
    }

    return (
        <DropDownContext.Provider value={
            {
                menuOpen,
                setMenuOpen,
                focused,
                setFocused,
                selected,
                setSelected,
                selectAndClose,
                childrenKey,
                selectedCategories,
                setSelectedCategories,
                callback
            }
        }>
            {children}
        </DropDownContext.Provider>
    )
}