import { createContext } from "react";
import { API_TIENDAS } from "../constants/constants";
import useSessionStorage from "../hooks/useSessionStorage";
export const SessionContext = createContext(null);

function SessionProvider({ children }) {
  const [session, setSession] = useSessionStorage("session", null);

  async function login(email, password) {
    try {
      await fetch(`${API_TIENDAS}/users?email=${email}&password=${password}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((resJson) => {
          setSession(resJson);
        });
    } catch (error) {
      console.error(error);
    }
  }

  async function signUp(email, password, name) {
    try {
      await fetch(`${API_TIENDAS}/users`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
          password,
          name,
        }),
      })
        .then((res) => res.json())
        .then((resJson) => {
          setSession(resJson);
        });
    } catch (error) {
      console.error(error);
    }
  }

  async function logout() {
    setSession(null);
    return Promise.resolve();
  }

  const value = {
    session,
    login,
    signUp,
    logout,
  };

  return (
    <SessionContext.Provider value={value}>{children}</SessionContext.Provider>
  );
}

export default SessionProvider;
