import { useState } from "react";
import categories from "../../data/categories";
import types from "../../data/types";
import Card from "../components/Card";
import SlideView from "../components/SlideView";
function CategoriesPage() {
  const [type, setType] = useState("all");
  const [category, setCategory] = useState(null);

  return (
    <div className="w-full h-full min-h-screen bg-neutral-950 pt-20 text-white flex flex-col place-items-center">
      <section>
        <h2 className="text-xl">Sección</h2>
        <SlideView>
          {types.map(({ name, image }) => {
            return (
              <button
                key={name}
                onClick={() => {
                  setType(name);
                  setCategory(null);
                }}
              >
                <Card image={image}>
                  <h3 className="text-xl p-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">{name}</h3>
                </Card>
              </button>
            );
          })}
        </SlideView>
      </section>
      <section>
        <h2 className="text-xl">Categoría</h2>
        <SlideView>
          {categories
            .filter((category) => type === category.type || type === "all")
            .map((category) => {
              return (
                <button
                  key={category.name}
                  onClick={() => {
                    setCategory(category);
                  }}
                >
                  <Card image={category.image}>
                    <h3 className="text-xl p-2">{category.name}</h3>
                  </Card>
                </button>
              );
            })}
        </SlideView>
      </section>
      {category?.sub_categories && <section>
        <h2 className="text-xl">Sub Categoría</h2>
        <SlideView>
          {category.sub_categories.map((sub_category) => {
            return (
              <button key={sub_category.name} onClick={() => {
                setCategory(sub_category)
              }}>
                <Card image={sub_category.image}>
                  <h3 className="text-xl p-2">{sub_category.name}</h3>
                </Card>
              </button>
            );
          })}
        </SlideView>
      </section>}
    </div>
  );
}

export default CategoriesPage;
