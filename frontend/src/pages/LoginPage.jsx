import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import useSession from "../hooks/useSession";

function LoginPage() {
  const [correo, setCorreo] = useState("");
  const [clave, setClave] = useState("");
  const [session, login] = useSession();
  const navigate = useNavigate(null);

  useEffect(() => {
    if (session) {
      navigate("/productos");
    }
  }, [session, navigate]);

  return (
    <div className="w-full h-full min-h-screen grid place-items-center bg-neutral-900">
      <form
        onSubmit={handleSubmit}
        className="rounded-xl p-5 bg-neutral-300 min-w-[20rem]"
      >
        <div className="flex flex-col">
          <label htmlFor="correo" className="font-semibold">
            Correo:
          </label>
          <input
            type="email"
            className="p-2 rounded w-full"
            id="correo"
            value={correo}
            onChange={handleCorreoChange}
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="clave" className="font-semibold mt-5">
            Clave:
          </label>
          <input
            type="password"
            className="p-2 rounded w-full"
            id="clave"
            value={clave}
            onChange={handleClaveChange}
          />
        </div>
        <button
          type="submit"
          className="rounded-lg bg-violet-500/70 text-white  font-semibold w-full py-2 mt-5"
        >
          Iniciar Sesión
        </button>
        <button className="rounded-lg bg-violet-500/70 text-white  font-semibold w-full py-2 mt-2">
          <Link to="/sign-up">Crear Cuenta</Link>
        </button>
      </form>
    </div>
  );

  function handleCorreoChange(e) {
    setCorreo(e.target.value);
  }

  function handleClaveChange(e) {
    setClave(e.target.value);
  }

  async function handleSubmit(e) {
    e.preventDefault();

    login(correo, clave);
  }
}

export default LoginPage;
