import { useNavigate } from "react-router-dom";
import useSession from "../hooks/useSession";
import { useEffect } from "react";

function Home() {
  const [session] = useSession();
  const navigate = useNavigate();
  useEffect(() => {
    if (!session) {
      navigate("/login");
    } else {
      navigate("/productos");
    }
  });

  return (
    <div className="w-full h-full min-h-screen grid place-items-center bg-neutral-800 text-white text-3xl">
      Bienvenido a nuestra tienda colaborativa!
    </div>
  );
}

export default Home;
