import { useEffect, useState } from "react";
import SlideView from "../components/SlideView";
import Card from "../components/Card";

function ManagerPage() {
  const [stores, setStores] = useState([]);
  const [newStoreModalOpen, setNewStoreModalOpen] = useState(false);
  useEffect(() => {
    fetch("localhost:8081/stores", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((fetchedStores) => fetchedStores.json())
      .then((fetchedStores) => {
        setStores(fetchedStores);
      });
  }, []);

  return (
    <div className="w-full h-full min-h-screen flex flex-col place-items-center  bg-neutral-900">
      <section className="bg-neutral-700 p-5 rounded-lg max-w-5xl w-full mt-32">
        <div className="flex text-white">
          <h2 className="text-xl">Tiendas</h2>
          <button
            className="border-2 border-white rounded-lg py-2 px-4 ml-auto"
            onClick={() => {
              setNewStoreModalOpen(true);
            }}
          >
            Crear Tienda
          </button>
        </div>
        {stores.length > 0 ? (
          <SlideView>
            {stores.map((store) => {
              return (
                <button key={store.name} onClick={() => {}}>
                  <Card image="">
                    <h3 className="text-xl p-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                      {store.name}
                    </h3>
                  </Card>
                </button>
              );
            })}
          </SlideView>
        ) : (
          <p className="text-white">Aun no hay tiendas registradas</p>
        )}
      </section>
      {newStoreModalOpen && (
        <section className="absolute z-50 grid place-items-center backdrop-blur-lg w-full h-screen">
          <form className=" flex flex-col gap-3 mx-auto my-auto bg-neutral-700 max-w-2xl h-fit text-white p-5 border-2 border-neutral-500 rounded-lg shadow-lg shadow-neutral-500/30">
            <label className="flex flex-col">
              Nombre de la tienda
              <input className="px-2 py-1 rounded-lg max-w-lg w-full" />
            </label>
            <label className="flex flex-col">
              Tipo de la tienda
              <input className="px-2 py-1 rounded-lg max-w-lg w-full" />
            </label>
            <label className="flex flex-col">
              Descripción de la tienda
              <textarea
                className="px-2 py-1 rounded-lg max-w-md resize-none overflow-x-hidden overflow-y-auto"
                maxLength={200}
              />
            </label>
            <label className="flex flex-col">
              Id del emprendedor
              <input className="px-2 py-1 rounded-lg max-w-lg w-full" />
            </label>

            <button
              className="border-2 border-white rounded-lg py-2 px-4 mt-4 bg-gray-200/10 hover:bg-transparent"
              onClick={() => {
                setNewStoreModalOpen(false);
              }}
            >
              Enviar
            </button>
          </form>
        </section>
      )}
    </div>
  );
}

export default ManagerPage;
