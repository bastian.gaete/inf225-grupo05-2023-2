import { useState } from "react";
import stores from "../../data/stores";
import SlideView from "../components/SlideView";
import Card from "../components/Card";
import types from "../../data/types";
import GridView from "../components/GridView";
import products from "../../data/products";

function MyStorePage() {
  const [selectedStore, setSelectedStore] = useState(stores[0]);
  return (
    <div className="w-full h-full min-h-screen bg-neutral-950 pt-20 text-white flex flex-col place-items-center pb-10">
      <section>
        <h2 className="text-xl">Tiendas</h2>
        <SlideView>
          {stores.map((store) => {
            return (
              <button
                key={store.name}
                onClick={() => {
                  setSelectedStore(store);
                }}
              >
                <Card
                  image={types.find((type) => store.type === type.name)?.image}
                >
                  <h3 className="text-xl p-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                    {store.name}
                  </h3>
                </Card>
              </button>
            );
          })}
        </SlideView>
      </section>
      <section className="w-full max-w-7xl mt-10 flex">
        <Card
          image={types.find((type) => selectedStore.type === type.name)?.image}
        />
        <div className="w-full bg-neutral-800 rounded-r-lg p-4 pr-20">
          <h2 className="text-2xl font-semibold">{selectedStore.name}</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>
      </section>
      <section className="w-full max-w-7xl mt-10 flex flex-col bg-neutral-800 rounded-lg p-5 place-items-center">
        <div className="w-full  p-4 flex">
          <h2 className="text-2xl font-semibold">{selectedStore.name}&apos;s products</h2>
          <button className="border-2 border-white rounded-lg py-2 px-4 ml-auto">Añadir</button>
          <button className="border-2 border-white rounded-lg py-2 px-4 ml-4">Quitar</button>
        </div>
        <GridView>
          {products.map((product) => {
            return (
              <button key={product.name} to={`/products/${product.name}`}>
                <Card
                  image={
                    product.image ||
                    "https://assets.thehansindia.com/h-upload/2022/07/18/1303611-pro.webp"
                  }
                >
                  <h3 className="text-xl px-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                    {product.name}
                  </h3>
                  <h4 className="pb-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                    ${product.price}
                  </h4>
                </Card>
              </button>
            );
          })}
        </GridView>
      </section>
    </div>
  );
}

export default MyStorePage;
