import { useEffect, useState } from "react";
import hardcodedProducts from "../../data/products";
import GridView from "../components/GridView";
import Card from "../components/Card";
import { Link } from "react-router-dom";
import { BiSearchAlt } from "react-icons/bi";
import { MdOutlineFilterAlt, MdOutlineFilterAltOff } from "react-icons/md";
import { MAX_PRICE } from "../constants/constants";
import types from "../../data/types";
import MultilevelDropdown from "../components/MultilevelDropdown";
import categories from "../../data/categories";
import { DropDownContextProvider } from "../context/DropdownContext";

const ProductsPage = () => {
  const [filters, setFilters] = useState({
    name: "",
    type: "all",
    categories: [],
    max_price: MAX_PRICE,
  });

  const [openFilters, setOpenFilters] = useState(false);
  const [products, setProducts] = useState(hardcodedProducts);

  // const [products, setProducts] = useState(hardcodedProducts);

  useEffect(() => {
    if (openFilters) return;

    setFilters({
      name: "",
      type: "all",
      categories: [],
      max_price: MAX_PRICE,
    });
  }, [openFilters]);

  /*
    useEffect(() => {
        fetch("http://localhost:8080/products", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "User-Agent": "TiendaColaborativaClient",
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setProducts(data);
            });
    }, []);
*/

  const filteredProducts = products.filter(
    (product) =>
      product.name?.toLowerCase().includes(filters.name.toLowerCase()) &&
      (filters.type === "all" || filters.type === product.type) &&
      (filters.categories.length === 0 ||
        filters.categories.every((element) =>
          product.categories.includes(element.name)
        )) &&
      filters.max_price >= product.price
  );

  const noResultsFound = filteredProducts.length === 0;

  return (
    <div className="w-full h-full min-h-screen bg-neutral-950 pt-20 px-20 text-white flex flex-col place-items-center">
      <section className="px-5 bg-neutral-900 w-full">
        <div className="flex h-16 place-items-center ">
          <button
            className="text-2xl ml-auto mr-2"
            onClick={() => {
              setOpenFilters((prev) => !prev);
            }}
          >
            {openFilters ? (
              <MdOutlineFilterAltOff />
            ) : (
              <MdOutlineFilterAlt className="scale-110" />
            )}
          </button>
          <div className="w-fit relative">
            <input
              className="w-[14rem] h-10 p-2 rounded-lg text-black text-lg"
              onChange={(event) => {
                setFilters((prev) => {
                  return {
                    ...prev,
                    name: event.target.value,
                  };
                });
              }}
            />
            <BiSearchAlt className="absolute right-2 top-3 my-auto text-black" />
          </div>
        </div>
        {openFilters && (
          <div className="flex place-items-center gap-4 pb-4">
            <label className="flex flex-col place-content-center">
              <span className="font-semibold text-lg">
                Precio Máximo{" "}
                <small className="font-normal text-xs">
                  ${filters.max_price}
                </small>
              </span>
              <input
                type="range"
                min="0"
                max={MAX_PRICE}
                step={100}
                value={filters.max_price}
                onChange={(event) => {
                  setFilters((prev) => {
                    return {
                      ...prev,
                      max_price: parseInt(event.target.value),
                    };
                  });
                }}
              />
            </label>
            <label className="flex flex-col place-content-center">
              <span className="font-semibold text-lg">Tipo</span>
              <DropDownContextProvider
                callback={(selection) => {
                  console.log(selection);
                  setFilters((prev) => ({
                    ...prev,
                    type: selection[0].name,
                    categories: [],
                  }));
                }}
              >
                <MultilevelDropdown items={types} enabled={true} />
              </DropDownContextProvider>
            </label>
            <label className="flex flex-col place-content-center">
              <span className="font-semibold text-lg">Categoría</span>
              <DropDownContextProvider
                childrenKey="sub_categories"
                callback={(categories) => {
                  setFilters((prev) => ({ ...prev, categories }));
                }}
              >
                <MultilevelDropdown
                  items={categories?.filter((category) => {
                    return category.type === filters.type;
                  })}
                  enabled={filters.type !== "all"}
                />
              </DropDownContextProvider>
            </label>
          </div>
        )}
      </section>
      <section className="bg-neutral-800 mt-10 p-5 rounded-lg">
        <h2 className="text-xl font-semibold mb-3">Productos</h2>
        {noResultsFound ? (
          <p>Lo sentimos, no hay productos que cumplan con lo buscado.</p>
        ) : (
          <GridView>
            {filteredProducts.map((product) => {
              return (
                <Link key={product.name} to={`/products/${product.name}`}>
                  <Card
                    image={
                      product.image ||
                      "https://assets.thehansindia.com/h-upload/2022/07/18/1303611-pro.webp"
                    }
                  >
                    <h3 className="text-xl px-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                      {product.name}
                    </h3>
                    <h4 className="px-2 pb-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                      ${product.price}
                    </h4>
                  </Card>
                </Link>
              );
            })}
          </GridView>
        )}
      </section>
    </div>
  );
};

export default ProductsPage;
