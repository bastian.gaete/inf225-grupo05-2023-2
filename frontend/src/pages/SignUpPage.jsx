import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import useSession from "../hooks/useSession";

function SignUpPage() {
  const [session, , signUp] = useSession();
  const navigate = useNavigate();

  useEffect(() => {
    if (session) {
      navigate("/productos");
    }
  }, [session, navigate]);

  return (
    <div className="w-full h-full min-h-screen grid place-items-center bg-neutral-900">
      <form
        className="rounded-xl p-5 bg-neutral-300 w-[25rem] h-fit flex flex-col"
        onSubmit={handleSubmit}
      >
        <h1 className="text-xl font-bold text-center">
          Regístrate para acceder a funciones exclusivas!
        </h1>
        <div className="flex flex-col">
          <label className="font-semibold mt-2" htmlFor="name">
            Nombre:
          </label>
          <input className="p-2 rounded w-full" type="text" id="name" />
        </div>
        <div className="flex flex-col">
          <label className="font-semibold mt-2" htmlFor="apellido">
            Apellido:
          </label>
          <input className="p-2 rounded w-full" type="text" id="apellido" />
        </div>
        <div className="flex flex-col">
          <label className="font-semibold mt-2" htmlFor="correo">
            Correo:
          </label>
          <input className="p-2 rounded w-full" type="email" id="correo" />
        </div>
        <div className="flex flex-col">
          <label className="font-semibold mt-2" htmlFor="clave">
            Contraseña:
          </label>
          <input className="p-2 rounded w-full" type="password" id="clave" />
        </div>
        <button
          type="submit"
          className="rounded-lg bg-violet-500/70 text-white  font-semibold w-full py-2 mt-5"
        >
          Enviar
        </button>
        <Link
          to="/login"
          className="rounded-lg bg-violet-500/70 text-white font-semibold w-full py-2 mt-3 text-center"
        >
          Ya tengo una cuenta
        </Link>
      </form>
    </div>
  );

  async function handleSubmit(e) {
    e.preventDefault();
    const nombre = e.target[0].value;
    const apellido = e.target[1].value;
    const correo = e.target[2].value;
    const clave = e.target[3].value;

    signUp(correo, clave, nombre + " " + apellido);
  }
}

export default SignUpPage;
