import { useState } from 'react'
import types from '../../data/types';
import SlideView from '../components/SlideView';
import Card from '../components/Card';
import GridView from '../components/GridView';
import stores from "../../data/stores"
function StoresPage() {

  const [type, setType] = useState("all");

  return (
    <div className="w-full h-full min-h-screen bg-neutral-950 pt-20 text-white flex flex-col place-items-center pb-10">
      <section>
        <h2 className="text-xl">Sección</h2>
        <SlideView>
          {types.map(({ name, image }) => {
            return (
              <button
                key={name}
                onClick={() => {
                  setType(name);
                }}
              >
                <Card image={image}>
                  <h3 className="text-xl p-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">{name}</h3>
                </Card>
              </button>
            );
          })}
        </SlideView>
      </section>
      <section>
        <h2 className="text-xl">Tiendas</h2>
        <GridView>
          {stores.filter((store) => store.type === type || type === "all").map((store) => {
            return (
              <button
                key={store.name}
              >
                <Card image={types.find((type) => store.type === type.name)?.image}>
                  <h3 className="text-xl p-2 drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">{store.name}</h3>
                </Card>
              </button>
            );
          })}
        </GridView>
      </section>
    
    </div>
  )
}

export default StoresPage