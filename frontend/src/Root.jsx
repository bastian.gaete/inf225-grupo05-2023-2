import { Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar";
import CategoriesPage from "./pages/CategoriesPage";
import StoresPage from "./pages/StoresPage";
import ProductsPage from "./pages/ProductsPage";
import MyStorePage from "./pages/MyStorePage";
import LoginPage from "./pages/LoginPage";
import SignUpPage from "./pages/SignUpPage";
import Home from "./pages/Home";
import SessionProvider from "./context/SessionContext";
import ManagerPage from "./pages/ManagerPage";

function Root() {
  return (
    <SessionProvider>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/categorias" element={<CategoriesPage />} />
        <Route path="/tiendas" element={<StoresPage />} />
        <Route path="/productos" element={<ProductsPage />} />
        <Route path="/mis-tiendas" element={<MyStorePage />} />
        <Route path="/sign-up" element={<SignUpPage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/gestor" element={<ManagerPage />} />
      </Routes>
    </SessionProvider>
  );
}

export default Root;
