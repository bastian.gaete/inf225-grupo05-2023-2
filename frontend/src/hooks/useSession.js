import { useContext } from "react";
import { SessionContext } from "../context/SessionContext";

function useSession() {
  const { session, login, signUp, logout } = useContext(SessionContext);
  return [session, login, signUp, logout];
}

export default useSession;
