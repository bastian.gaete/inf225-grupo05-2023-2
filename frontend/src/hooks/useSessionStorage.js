import { useState, useEffect } from "react";

function useSessionStorage(key, initialValue) {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = window.sessionStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      return initialValue;
    }
  });

  useEffect(() => {
    window.sessionStorage.setItem(key, JSON.stringify(storedValue));
  }, [key, storedValue]);

  const setValue = (value) => {
    try {
      setStoredValue(value);
    } catch (error) {
      console.log(error);
    }
  };
  return [storedValue, setValue];
}

export default useSessionStorage;
