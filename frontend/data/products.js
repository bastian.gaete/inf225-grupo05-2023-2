const products = [
    {
        name: 'Free Shirt',
        type: "Clothing",
        categories: ["Shirts", "Men", "Woman"],
        price: 0
    },
    {
        name: 'T Shirt',
        type: "Clothing",
        categories: ["Shirts", "Clothing", "Men", "Women"],
        price: 5000
    },
    {
        name: 'Samsung Galaxy S23 Ultra',
        type: "Electronics",
        categories: ["Smartphones", "Samsung"],
        price: 800000
    },
    {
        name: 'Sink 3000',
        type: "Bathroom",
        categories: ["Sink"],
        price: 200000
    }
]

export default products;