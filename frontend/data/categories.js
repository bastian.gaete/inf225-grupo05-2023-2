const categories = [
    {   
        type: "Electronics",
        name: "Smartphones",
        image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list",
        sub_categories: [
            {name: "Apple", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Samsung", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Xiaomi", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
        ]
    },
    {   
        type: "Electronics",
        name: "Laptops",
        image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list",
        sub_categories: [
            {name: "Apple", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Samsung", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Xiaomi", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
        ]
    },
    {
        type: "Electronics",
        name: "Tablets",
        image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list",
        sub_categories: [
            {name: "Apple", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Samsung", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Xiaomi", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
        ]
    },
    {
        type: "Electronics",
        name: "Gaming Consoles",
        image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list",
        sub_categories: [
            {name: "Apple", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Samsung", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
            {name: "Xiaomi", image: "https://m-cdn.phonearena.com/images/article/64576-wide-two_940/The-Best-Phones-to-buy-in-2023---our-top-10-list"},
        ]
    },
    {
        type: "Home things",
        name: "Bathroom",
        image: "https://media.architecturaldigest.com/photos/5d2f3540dea3bc0008636368/16:9/w_2580,c_limit/After-Photo-7.jpg",
        sub_categories: [
            {name: "Shower", image: "https://media.architecturaldigest.com/photos/5d2f3540dea3bc0008636368/16:9/w_2580,c_limit/After-Photo-7.jpg"},
            {name: "Toilet", image: "https://media.architecturaldigest.com/photos/5d2f3540dea3bc0008636368/16:9/w_2580,c_limit/After-Photo-7.jpg"},
            {name: "Sink", image: "https://media.architecturaldigest.com/photos/5d2f3540dea3bc0008636368/16:9/w_2580,c_limit/After-Photo-7.jpg"},
        ]
    },
    {
        type: "Home things",
        name: "Kitchen",
        image: "https://media.houseandgarden.co.uk/photos/649188c6d6a55acd0396e5f0/4:3/w_2220,h_1665,c_limit/HG-HAM-KateCox06468-CREDIT-TomGriffiths-TomGPhoto.jpg",
        sub_categories: [
            {name: "Fridge", image: "https://media.houseandgarden.co.uk/photos/649188c6d6a55acd0396e5f0/4:3/w_2220,h_1665,c_limit/HG-HAM-KateCox06468-CREDIT-TomGriffiths-TomGPhoto.jpg"},
            {name: "Oven", image: "https://media.houseandgarden.co.uk/photos/649188c6d6a55acd0396e5f0/4:3/w_2220,h_1665,c_limit/HG-HAM-KateCox06468-CREDIT-TomGriffiths-TomGPhoto.jpg"},
            {name: "Microwave", image: "https://media.houseandgarden.co.uk/photos/649188c6d6a55acd0396e5f0/4:3/w_2220,h_1665,c_limit/HG-HAM-KateCox06468-CREDIT-TomGriffiths-TomGPhoto.jpg"},
        ]
    },
    {
        type: "Clothing",
        name: "Men",
        image: "https://img.freepik.com/free-photo/young-handsome-man-choosing-clothes-shop_1303-19720.jpg",
        sub_categories: [
            {name: "Shirts", image: "https://img.freepik.com/free-photo/young-handsome-man-choosing-clothes-shop_1303-19720.jpg"},
            {name: "Pants", image: "https://img.freepik.com/free-photo/young-handsome-man-choosing-clothes-shop_1303-19720.jpg"},
            {name: "Shoes", image: "https://img.freepik.com/free-photo/young-handsome-man-choosing-clothes-shop_1303-19720.jpg"},
        ]
    },
    {
        type: "Clothing",
        name: "Women",
        image: "https://cdn.shopify.com/s/files/1/0441/3580/9179/files/ClothesMentor_HowitWorks.jpg?v=1621268736%0A",
        sub_categories: [
            {name: "Shirts", image: "https://cdn.shopify.com/s/files/1/0441/3580/9179/files/ClothesMentor_HowitWorks.jpg?v=1621268736%0A"},
            {name: "Pants", image: "https://cdn.shopify.com/s/files/1/0441/3580/9179/files/ClothesMentor_HowitWorks.jpg?v=1621268736%0A"},
            {name: "Shoes", image: "https://cdn.shopify.com/s/files/1/0441/3580/9179/files/ClothesMentor_HowitWorks.jpg?v=1621268736%0A"},
        ]
    }
]

export default categories;