const stores = [
    {
      "name": "Tech World",
      "type": "Electronics"
    },
    {
      "name": "Home Essentials",
      "type": "Home things"
    },
    {
      "name": "Fashion Trends",
      "type": "Clothing"
    },
    {
      "name": "Furniture Palace",
      "type": "Furniture"
    },
    {
      "name": "Bookworm's Paradise",
      "type": "Books"
    },
    {
      "name": "Toyland",
      "type": "Toys"
    },
    {
      "name": "Sports Zone",
      "type": "Sports"
    },
    {
      "name": "Beauty Bliss",
      "type": "Beauty"
    },
    {
      "name": "JewelCraft",
      "type": "Jewelry"
    },
    {
      "name": "Food Haven",
      "type": "Food"
    }
  ]

  export default stores;