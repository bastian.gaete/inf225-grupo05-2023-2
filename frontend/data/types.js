
const types = [
    {
      "name": "Electronics",
      "image": "https://media.istockphoto.com/id/1372577388/photo/old-electronic-devices-on-a-dark-background-the-concept-of-recycling-and-disposal-of.jpg?s=612x612&w=0&k=20&c=RGm3eCA76Y_IJJYGCLWS9acSR39Gb7iqsC_DIhJyG2g="
    },
    {
      "name": "Home things",
      "image": "https://img.freepik.com/free-photo/top-view-ecological-products-concept_23-2148781889.jpg?size=626&ext=jpg&ga=GA1.1.386372595.1697846400&semt=ais"
    },
    {
      "name": "Clothing",
      "image": "https://www.thoughtco.com/thmb/ctxxtfGGeK5f_-S3f8J-jbY-Gp8=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/close-up-of-clothes-hanging-in-row-739240657-5a78b11f8e1b6e003715c0ec.jpg"
    },
    {
      "name": "Furniture",
      "image": "https://classy.co.in/wp-content/uploads/2023/02/Single-Chairs.webp"
    },
    {
      "name": "Books",
      "image": "https://i.guim.co.uk/img/media/d305370075686a053b46f5c0e6384e32b3c00f97/0_50_5231_3138/master/5231.jpg?width=1200&quality=85&auto=format&fit=max&s=dfc589d3712148263b1dd1cb02707e91"
    },
    {
      "name": "Toys",
      "image": "https://www.ikea.com/gb/en/images/products/lillabo-toy-figure__0957753_ph164699_s5.jpg"
    },
    {
      "name": "Sports",
      "image": "https://lrabm.files.wordpress.com/2020/06/sports-image-low-res.jpg"
    },
    {
      "name": "Beauty",
      "image": "https://cdn.logojoy.com/wp-content/uploads/20191023114758/AdobeStock_224061283-min.jpeg"
    },
    {
      "name": "Jewelry",
      "image": "https://i0.wp.com/www.elf925.com/blog/wp-content/uploads/AdobeStock_270733509-scaled.jpeg?fit=2560%2C1707&ssl=1"
    },
    {
      "name": "Food",
      "image": "https://www.adityabirlacapital.com/healthinsurance/active-together/wp-content/uploads/2018/10/Unhealthy-Food-Chart.jpg"
    }
  ]

export default types;