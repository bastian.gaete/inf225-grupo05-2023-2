# Grupo 5

Este es el repositorio del *Grupo 5*, cuyos integrantes son:

* Bastián Gaete Aguilera - 201803008-2.
* Cristóbal Benavides Proboste - 202073106-3.
* Felipe Márquez Millán - 201803016-3.
* Gonzalo Severín Muñoz - 202073088-1.
* **Tutor**: Camila Norambuena.

## Wiki

Se puede acceder a la wiki mediante el siguiente [enlace](https://gitlab.com/bastian.gaete/inf225-grupo05-2023-2/-/wikis/home).

## Videos

* [Video - Presentación del Cliente](https://youtu.be/NI3GSDZrnHY)
* [Video - Presentación de los Desarrolladores - Hito #1](https://youtu.be/XsIEsyb7puA)
* [Video - Presentación de los Desarrolladores - Hito #4](https://youtu.be/XotRjBSgaiQ)
* [Video - Presentación de los Desarrolladores - Hito#7](https://youtu.be/MveE7kiyzIg)

## Aspectos Técnicos Relevantes

Para el front:
* Dentro de carpeta frontend, ejecutar los siguientes comandos en consola:
```
npm install
npm run dev
```

Para las APIs:
* Dentro de carpeta proy-base y con Docker abierto, ejecutar los siguientes comandos en consola:
```
npm install @prisma/client
docker network create my-network
docker compose up
```
